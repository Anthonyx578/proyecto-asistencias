import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//Cargar Scripts
import{CargarScriptsService} from "./cargar-scripts.service"
//Crgar Scripts
//Rutas
import { app_routing } from './app.routes';
//Rutas
//Inicio Importaciones de Componentes
import { AppComponent } from './app.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { LoginComponent } from './components/login/login.component';
import { HelpComponent } from './components/help/help.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RecoverPasswordComponent } from './components/recover-password/recover-password.component';
import { RegisterComponent } from './components/register/register.component';
import { SemestersComponent } from './components/semesters/semesters.component';
import { SubjectsComponent } from './components/subjects/subjects.component';
//Fin Importaciones de Componentes
import { initializeApp,provideFirebaseApp } from '@angular/fire/app';
import { environment } from '../environments/environment';
//Inicio importaciones de FireStore
import { provideFirestore,getFirestore } from '@angular/fire/firestore';
import { provideAuth,getAuth } from '@angular/fire/auth';
//Fin importaciones de FireStore
//Inicio Importaciones de Forms
import{FormsModule,NgForm,NgModel} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
//Fin Importaciones de Forms
@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    LoginComponent,
    HelpComponent,
    ProfileComponent,
    RecoverPasswordComponent,
    RegisterComponent,
    SemestersComponent,
    SubjectsComponent
  ],
  imports: [
    BrowserModule,
    //Forms Module Imports
    ReactiveFormsModule,
    FormsModule,
    //Forms Module Imports
    app_routing,
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
    provideAuth(()=>getAuth())
  ],
  providers: [
    //import para cargar Scripts
    CargarScriptsService
    //import para cargar Scripts
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
