import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CargarScriptsService {

  constructor() { }

  Carga(Archivos:string[])
  {
    for (let Script of Archivos)
    {
      let script = document.createElement('script');
      script.src = "./assets/JS/"+ Script + ".js";
      let body= document.getElementsByTagName("body")[0];
      body.appendChild(script);
    }
  }
}
