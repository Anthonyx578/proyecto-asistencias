import { Component } from '@angular/core';
import {RouterModule,Routes}from '@angular/router';
import {
    LoginComponent,
    RegisterComponent,
    MainMenuComponent,
    RecoverPasswordComponent,
    ProfileComponent,
    SubjectsComponent,
    SemestersComponent,
    HelpComponent

} 
from "./components/exports_paginas";
//importar lo que se necesita para bloquear los componentes
import { canActivate,redirectUnauthorizedTo } from '@angular/fire/auth-guard';

const app_routes:Routes=
[
    {path:'Login',component:LoginComponent},
    {path:'Register',component:RegisterComponent},
    {path:'RecoverPassword',component:RecoverPasswordComponent},
    
    {path:'MainMenu',component:MainMenuComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/login']))
    },
    {path:'Profile',component:ProfileComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/login']))
    },
    {path:'Subjects',component:SubjectsComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/login']))
    },
    {path:'Semesters',component:SemestersComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/login']))
    },
    {path:'Help',component:HelpComponent,
    ...canActivate(()=>redirectUnauthorizedTo(['/login']))
    },
    {path:'**',pathMatch:'full',redirectTo:'Login'}
];

export const app_routing = RouterModule.forRoot(app_routes,{useHash:true});