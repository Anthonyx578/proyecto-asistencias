import { Firestore,collection,addDoc,collectionData } from '@angular/fire/firestore';
import { UsersService } from 'src/app/services/users.service';
import{FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  formLogin:FormGroup;
  constructor(private login:UsersService, private ruta:Router)
  {
    this.formLogin = new FormGroup({
      email:new FormControl(),
      password:new FormControl()
    })

  }
  onSubmit()
  {
    this.login.login(this.formLogin.value)
    .then(response=>{
      this.ruta.navigate(['/MainMenu']);
    })
    .catch(error=>alert("Contraseña y/o correo electronico incorrecto"));
  }
}
