import { Component, OnInit } from '@angular/core';
import { stringLength } from '@firebase/util';
//Import para leer JS
import { CargarScriptsService } from 'src/app/cargar-scripts.service';
//Import para leer JS
//Imports de Base de Datos
import { Firestore,collection,addDoc,collectionData } from '@angular/fire/firestore';
//Imports de Base de Datos
//Imports de Forms
import{FormGroup,FormControl,FormBuilder,Validators} from '@angular/forms';
//Imports de Forms
//import de Users Service
import { UsersService } from 'src/app/services/users.service';
import { PlacesService } from 'src/app/services/places.service';
//import de Users Service
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{
  FormRegister:FormGroup;
  constructor
  (
    private Carga:CargarScriptsService, 
    //cargar los servicios de base de datos
    private Places:PlacesService, 
    private User:UsersService,
    private ruta:Router
  )
  {
    //Cargar JS
    Carga.Carga(["register"])

    //Cargar JS

    this.FormRegister = new FormGroup
    ({
      email:new FormControl(),
      password:new FormControl(),
      Cpassword:new FormControl(),
      nombre:new FormControl(),
      apellido:new FormControl(),
      cedula:new FormControl(),
      nsemestre:new FormControl()
    })
  } 
  ngOnInit(): void {
  }



  async Register(){
    
    const response= await this.User.register(this.FormRegister.value)
    .then(response=>
    {
     console.log(response)
    })
    .catch(error=>console.log(error));
  }
  async LoginBD(){
   const response = await this.Places.addPlace(this.FormRegister.value);
    console.log(response);
  }

  //Metodo para enviar
  async onSubmit()
  {
    this.validar();
    this.Register();
    this.LoginBD();
  }

  validar()
  {
     /*Variables*/
     const Correo = this.FormRegister.value.email;
     const contrasena= this.FormRegister.value.password;
     const Ccontrasena=this.FormRegister.value.Cpassword;
     const name= this.FormRegister.value.name;
     const lastName= this.FormRegister.value.lastName;
     const NumbCeluda= this.FormRegister.value.NumbCeluda;
 
 
     const valico= /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
     const validar= valico.test(Correo); 
     //Validacion de Email
     if(validar==true)
     {
         //Validacion de contraseñas
         if(contrasena ==null || contrasena.length == 0)
         { //Validar que el campo contraseña no esta vacio
             alert("El campo contraseña no puede estar vacio")
         }
         else
         {
             if(Ccontrasena==null || Ccontrasena.length == 0)
             {//Validar que el campo confirmar contraseña no esta vacio
                 alert("El campo confirmar contraseña no puede estar vacio") 
             }
             else
             {
                 if(name==null ||name.length == 0)
                 {//validar que el nombre no sea nulo
                     alert('El campo Nombre no puede estar vacio')
                 }
                 else
                 {
                     if(lastName == null || lastName.length == 0)
                     {
                         alert('El campo apellido no puede estar vacio')
                     }
                     else{
                         if(contrasena == Ccontrasena )
                         {
                             if(NumbCeluda.length== 10)
                             {
                                 alert("Registro exitoso");
                                 this.ruta.navigate(['/Login']);
                                
                             }
                             else
                             {
                                 alert("numero de cedula no valido");
                             }
                     
                         }else
                         {
                             alert("Contraseñas distintas");
                             
                         }
                     }
 
                 }
             }
         }
     }else
     {
         alert("Correo electronico no valido");
     }
  }
 

}
 
