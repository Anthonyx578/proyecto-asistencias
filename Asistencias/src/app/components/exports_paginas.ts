export{LoginComponent} from "./login/login.component";
export{RegisterComponent} from "./register/register.component";
export{HelpComponent} from "./help/help.component";
export{MainMenuComponent} from "./main-menu/main-menu.component";
export{ProfileComponent} from "./profile/profile.component";
export{RecoverPasswordComponent} from "./recover-password/recover-password.component";
export{SemestersComponent} from "./semesters/semesters.component";
export{SubjectsComponent} from "./subjects/subjects.component";