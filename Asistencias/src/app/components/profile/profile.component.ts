import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  constructor(private Logout:UsersService,private ruta:Router){

  }
  logout(){
    this.Logout.logout()
    .then(()=>{
      this.ruta.navigate(['/login'])
    })
    .catch(error=>console.log(error));
  }

}
